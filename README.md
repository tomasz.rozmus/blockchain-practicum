# Blockchain - Practicum

This is the main repo of blockchain-practicum course

## https://gitlab.com/tomasz.rozmus/blockchain-practicum

## Classes
1: Introduction
 * [Course draft](packages/01_introduction/docs/00_course_plan.md)
 * [Environment preparation](packages/01_introduction/docs/01_environment.md)
 * [Git version control system](packages/01_introduction/docs/01_git.md)
 * [Node.js](packages/01_introduction/docs/02_nodejs.md)
 * [NPM project](packages/01_introduction/docs/03_npm.md)
 * [Test Driven Development](packages/01_introduction/docs/06_tdd.md)
2. Proof of Work
 * [Hash](packages/02_pow/docs/00_hash.md)
 * [POW](packages/02_pow/docs/01_pow.md)
 * [Yarn](packages/02_pow/docs/02_yarn.md)
 * [POW extended](packages/02_pow/docs/03_pow_extended.md)
 * [POW Timings](packages/02_pow/docs/04_timings.md)
 * [POW Calibration](packages/02_pow/docs/06_autocalibration.md)
3. Merkle Tree
 * [Merkle tree](packages/03_merkle_tree/docs/01_merkle_tree.md)
4. Blockchain - basics 
 * [Blockchain - class](packages/04_blockchain/docs/01_blockchain_class.md)
5. Blochchain - network node
 * [Blockchain - network node](packages/05_blockchain_node/docs/01_blockchain_node.md)
6. Ethereum - basics
 * [Metamask](packages/06_ethereum_basics/docs/01_metamask.md)