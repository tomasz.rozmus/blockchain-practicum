# Project Proposals
Here is a list of proposed projects. 2-4 students for each project. Project should consist of user interface (web application, phone application, desktop app, terminal, etc) and associated smart-contracts on Ethereum blockchain. Contracts can be deployed to test networks only. Contracts must have tests. UI must have at least unit tests. Solution code should be on (gitlab/github/bitbucket/etc). Smaller projects should have better quality (more code coverage, code linting, some documentation, in code comment, etc).

Project can be extended with more functions (and more people). You can propose similar projects. Basically everything involving smart-contracts can be a project.

## Proposals
### 1. Housing association voting app. (3 students) 
Smart Contract requirements:
Users will have a right to vote on specific spending and reaching consensus about expenses. 
Contract will collect the money (rent). 
Users will be penalized for delaying payment of their cooperative rent by decreasing their vote share or losing right to vote temporary.
Users can propose fund spending ideas (i.e. let's buy a trampoline) or simple, cost-free ideas (i.e. let's take turns in watering the lawn).

### 2. Vehicle ownership database (car, bike). (3 students) 
Smart Contract requirements:
Vehicle id can be VIN for cars/Serial number for bikes.
Adding new vehicle and user must be confirmed by 2 existing users.
Current and next owner must agree on a transfer.
There must be an option for vehicle utilisation - confirmed by owner and one other user.
Extra: can be connected somehow to project 7.

### 3. Crypto lottery. (2 students) 
Smart Contract requirements:
Create a pool which collects small amount of ether from users and picks a winner every N bets.
Contract owner can change N at any time.
Contract owner can change ethereum range at any time.
Winning chance should be proportional to ether transferred.

### 4. European stock options on ERC20/ERC721 Tokens. The financial part. (4 students)

### 5. European stock options on ERC20/ERC721 Tokens. Source of truth for exercise price. (4 students)

### 6. Citizen address management app. (3 students)
Smart Contract requirements:
User can have 2 addresses: address for correspondence and registered address.
User/address identification is PESEL.
Claiming/changing correspondence address require approval of another user.
Claiming/changing registered address require approval of authoritative user or contract owner.
Contract owner can make some user authoritative.

### 7. Car technical review database. (3 students) 
Smart Contract requirements:
Vehicle id is VIN.
Contract owner can add authoritative user (mechanic, technician).
Contract owner can add superauthoritative user (police).
Authoritative users can add vehicles.
Car mileage is registered upon every review. Car mileage is irremovable.
Review defects (things to fix) are saved in 2 kind of storage.
Permanent storage for serious failures.
Temporary storage for small defects. Small defects not fixed until next review became permanent.
Police can register a crash event in permanent storage.
Extra: can be connected somehow to project 2.

