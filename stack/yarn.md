# Yarn

Yarn is other package manager for JavaScript. Similar to npm, with some improvements.
* faster - has internal cache on the client, can be used in offline mode
* more secure - uses checksums (hashes) before code execution to prevent malicious code injection
* more reliable
    * network failure for single dependency wont crash whole install
    * avoids creating duplicates (semver)
    * guarantees same configuration of packages regardles of OS and install order
* has *workspaces*

