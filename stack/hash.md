# Hash functions
A hash function is any function that can be used to map data of arbitrary size onto data of a fixed size. (Wikipedia)

## Cryptographic hash function
Cryptographic hash function is a special class of hash function that has certain properties which make it suitable for use in cryptography. It is a mathematical algorithm that maps data of arbitrary size to a bit string of a fixed size (a hash) and is designed to be a one-way function, that is, a function which is infeasible to invert. (Wikipedia)
 
 The ideal cryptographic hash function has five main properties:
 * it is deterministic so the same message always results in the same hash
 * it is quick to compute the hash value for any given message
 * it is infeasible to generate a message from its hash value except by trying all possible messages
 * a small change to a message should change the hash value so extensively that the new hash value appears uncorrelated with the old hash value
 * it is infeasible to find two different messages with the same hash value (Wikipedia)