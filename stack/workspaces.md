## yarn workspaces

Workspaces allows you to setup multiple packages in such a way that you only need to run `yarn install` once to install all of them in a single pass. Your dependencies can be linked together, which means that your workspaces can depend on one another while always using the most up-to-date code available. Project dependencies will be installed together, giving Yarn more latitude to better optimize them.

https://yarnpkg.com/lang/en/docs/workspaces/