# Proof of Work

Proof of work describes a system that requires a *not-insignificant but feasible amount of effort* in order to deter frivolous or malicious uses of computing power, such as sending spam emails or launching denial of service attacks. (Investopedia)

A Proof-of-Work (PoW) system (or protocol, or function) is an economic measure to deter denial of service attacks and other service abuses such as spam on a network by *requiring some work* from the service requester, usually meaning processing time by a computer. 
... A key feature of these schemes is their asymmetry: the work must be moderately hard (but feasible) on the requester side but easy to check for the service provider. (Wikipedia)