const { sha256 } = require('utils')
const merkleTreeString = require('./exercises/merkleTreeString')

describe('merkleTreeString', () => {
  it('works for simplest data', () => {
    const data = 'aa'
    const root = merkleTreeString(data)
    const hash = sha256(sha256(data) + sha256(data))
    expect(root).toEqual(hash)
    expect(root).toEqual('360ef99155438d9c5413e82e1dcb0574e52d3db30dc71fbc183bdb5a44f3775e')
  })

  it('works 1', () => {
    const data = 'aaaa'
    const root = merkleTreeString(data)
    expect(root).toEqual('360ef99155438d9c5413e82e1dcb0574e52d3db30dc71fbc183bdb5a44f3775e')
  })

  it('works 2', () => {
    const data = 'bbbb'
    const root = merkleTreeString(data)
    expect(root).toEqual('444ea54180131b0ba4036825e925fb8804d36471f4e15f551d83b4ff440a229f')
  })

  it('works for more complex data', () => {
    const data = 'aaaabb'
    const root = merkleTreeString(data)
    expect(root).toEqual('e6b2d186a83e3057542fce7bf5fda349d0097d3241eace82dc11c8d826e4cee7')
  })

  it('works for very complex data', () => {
    const data = 'Big brown fox jumps over a lazy dog!'
    const root = merkleTreeString(data)
    expect(root).toEqual('ad8d66cc0b0b0f77cbabe9df63768acb167c1101f9c34e08cdf4be237dc404e8')
  })
})