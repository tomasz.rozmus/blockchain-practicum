# Merkle tree

[Definition](./../../../stack/merkle_tree.md)

https://en.wikipedia.org/wiki/Merkle_tree

You can read about its usage in Ethereum here:
https://blog.ethereum.org/2015/11/15/merkling-in-ethereum/

Exercise: Create `merkleTreeString` function which takes a string, cuts it to 2 letter words (blocks), and returns its Merkle tree root.
Use merkleTreeString.test.js for validation. If blocks length is odd, duplicate the last block to make it even.
Use answer from this link to solve a case when blocks (words) number is not a power of 2.
https://bitcoin.stackexchange.com/questions/46767/merkle-tree-structure-for-9-transactions

```
function merkleTreeString(data){
    // ...
    return treeRoot
}
```

Exercise: Create `merkleTree` by extending `merkleTreeString` function. Allow data to be an array of objects. Before hashing, convert every object to string with `JSON.stringify()` function.
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify

```
function merkleTree(data){
    // ...
    return treeRoot
}
```
