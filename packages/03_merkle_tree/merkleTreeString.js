const { sha256 } = require('utils')

function hashPairs (blocks) {
  let newBlocks = []
  for (let i = 0; i < blocks.length; i += 2) {
    let contentToHash = blocks[i]
    if (i + 1 < blocks.length) {
      contentToHash += blocks[i + 1]
    }
    else {
      contentToHash += blocks[i]
    }
    newBlocks.push(sha256(contentToHash))
  }
  if (newBlocks.length > 1) {
    return hashPairs(newBlocks)
  }
  return newBlocks[0]
}

function merkleTreeString (data) {
  const blocks = data.match(/.{1,2}/g)
  if (blocks.length % 2 !== 0) {
    blocks.push(blocks[blocks.length - 1])
  }
  const hashedBlocks = blocks.map(sha256)
  return hashPairs(hashedBlocks)
}

module.exports = merkleTreeString