# TDD
[Basic info](./../../../stack/tdd.md)

## Test Runner/Framework
A test runner (or test framework) is the library or tool that picks up an assembly (or a source code directory) that contains unit tests and then executes them and writes the test results to the console or log files.

We will use `jest` test framework https://jestjs.io/

```
const add = (a, b) => a + b
test('adds', () => {
    expect(add(1,2)).toEqual(3)
})

```

A good test should be structured into 3 groups:
* arrange
* act
* assert

It is called [triple A rule](https://medium.com/@pjbgf/title-testing-code-ocd-and-the-aaa-pattern-df453975ab80). Follow this rule. It is always appreciated by tech leads.

```
test('is best when properly structured', () => {
    // Arrange
    const a = 3
    const b = 4
    // Act
    const sum = add(a, b)
    // Assert
    expect(sum).toEqual(7)
})
```

Excercise: create a `multiplicativeHash` function using TDD approach. Implement https://en.wikipedia.org/wiki/Hash_function#Multiplicative_hashing 

Excercise: create a `proofOfWork` function using TDD approach.
```
 function proofOfWork(block, difficulty){
    // ...
    return nonce
 }
```

Excercise: create a `calibrateDifficulty` function which guarantees that your previous function executes on average between `min` and `max` milliseconds in 90% cases.

```
function calibrateDifficulty(minMilliseconds, maxMilliseconds) {
    // ...
    return difficulty
}

```