const add = require('./add')

// describe('add', () => {
test('works', () => {
  expect(add(1, 2)).toEqual(3)
})
test('works with one argument', () => {
  expect(add(7)).toEqual(7)
})
test('works when properly structured', () => {
  //arrange
  const a = 3
  const b = 4
  //act
  const sum = add(a, b)
  //assert
  expect(sum).toEqual(7)
})
// })