# Using web3 on the backend

This is an example on how to use web3 providers to get data on the backend. This provider has no signing capabilities.

Exercise: Extend api with methods for:
* getting ETH balance of a given address
* getting TT balance of a given address 

Exercise: Extend the previous with '/faucet/:address' endpoint. A call to faucet should result in transferring 1 TT to the address.
Tip: You need HDWalletProvider with your mnemonic and you need to create separate web3 instance.