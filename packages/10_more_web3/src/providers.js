const Web3 = require('web3')
const HDWalletProvider = require("truffle-hdwallet-provider");

const metadata = require('09_web3/build/contracts/TutorialToken')

const infuraUrl = 'https://ropsten.infura.io/v3/370de4d612ca47efaf482d0572656d6f'

// Standard provider
const web3Provider = new Web3.providers.HttpProvider(infuraUrl)
const web3 = new Web3(web3Provider)

// Provider with automatic signing
const mnemonic = "river cloth outdoor result share peace dinner web humble foil celery tuition";
const hdWalletProvider = new HDWalletProvider(mnemonic, infuraUrl);
const web3Signer = new Web3(hdWalletProvider)

// Contract metadata needed for contract instance
const contractAddress = metadata.networks[3].address
// const contractAddress = '0x9d69afa0454062dd3fda2f51f68b371a4958a9ea'
const abi = metadata.abi

// contract instance getters with caching
let contract = null
async function getContract () {
  if(contract === null){
    contract = await new web3.eth.Contract(abi, contractAddress)
  }
  return contract
}

let signerContract = null
async function getSignerContract () {
  if(signerContract === null){
    signerContract = await new web3Signer.eth.Contract(abi, contractAddress)
  }
  return signerContract
}

module.exports.getContract = getContract
module.exports.getSignerContract = getSignerContract
module.exports.web3 = web3
module.exports.web3Signer = web3Signer
module.exports.contractAddress = contractAddress
