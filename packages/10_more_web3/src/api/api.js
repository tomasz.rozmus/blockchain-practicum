const express = require('express')
const bodyParser = require('body-parser')
const { getContract, getSignerContract, web3, web3Signer, contractAddress } = require('../providers')

const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.get('/', async (req, res) => {
  res.send('Hello World')
})

app.get('/eth-balance/:address', async (req, res) => {
  const balance = web3.utils.fromWei(await web3.eth.getBalance(req.params.address))
  res.json({ balance: balance })
})

app.get('/eth-faucet/:address', async (req, res) => {
  const [account] = await web3Signer.eth.getAccounts()
  const transferred = await web3Signer.eth.sendTransaction({ from: account, to: req.params.address, value: web3.utils.toWei("0.05", "ether"), gas: 1000000 })
  res.json({ transferred: transferred })
  // res.json({ contract })
})

app.get('/tt-total', async (req, res) => {
  const contract = await getContract()
  const result = (await contract.methods.totalSupply.call()).toNumber()
  console.log(result)
  res.json({ totalSupply: result })
})

app.get('/tt-balance/:address', async (req, res) => {
  const contract = await getContract()
  const result = (await contract.methods.balanceOf(req.params.address).call()).toNumber()
  console.log(result)
  res.json({ balance: result })
})

app.get('/tt-faucet/:address', async (req, res) => {
  const contract = await getSignerContract()
  const [account] = await web3Signer.eth.getAccounts()
  const transferred = await contract.methods.transfer(req.params.address, 1).send({ from: account, gas: 1000000 })
  res.json({ transferred: transferred })
  // res.json({ contract })
})

app.get('/tt-faucet-hash/:address', async (req, res) => {
  const contract = await getSignerContract()
  const [account] = await web3Signer.eth.getAccounts()
  contract.methods.transfer(req.params.address, 1).send({ from: account, gas: 1000000 })
    .on('transactionHash', (hash) => {
      console.log('TRANSACTION_HASH')
      console.log(hash)
      res.json({ transactionHash: hash })
    })
    .on('error', (error, receipt, confirmations) => {
      console.log('ERROR')
      console.log(error)
      console.log(receipt)
      console.log(confirmations)
      res.json({ error, receipt, confirmations })
    })
})

app.get('/tt-faucet-steps/:address', async (req, res) => {
  const contract = await getSignerContract()
  const [account] = await web3Signer.eth.getAccounts()
  let response = {}
  contract.methods.transfer(req.params.address, 1).send({ from: account, gas: 1000000 })
    .on('transactionHash', (hash) => {
      console.log('TRANSACTION_HASH')
      console.log(hash)
      response.hash = hash
    })
    .on('confirmation', (confirmations, receipt) => {
      console.log('CONFIRMATION')
      console.log(confirmations)
      console.log(receipt)
      response.confirmations = confirmations
    })
    .on('receipt', (receipt) => {
      console.log('RECEIPT')
      console.log(receipt)
      response.receipt = receipt
    })
    .on('error', (error, receipt, confirmations) => {
      console.log('ERROR')
      console.log(error)
      console.log(receipt)
      console.log(confirmations)
      response.error = error
    })
  res.json(response)
})

app.get('/tt-faucet-raw/:address', async (req, res) => {
  const contract = await getSignerContract()
  const [account] = await web3Signer.eth.getAccounts()
  console.log('account', account)
  // Determine the nonce
  const count = await web3Signer.eth.getTransactionCount(account)
  console.log(`num transactions so far: ${count}`)
  // Sender token supply before sending
  const balance = await contract.methods.balanceOf(account).call()
  console.log(`Balance before send: ${balance}`)

  const rawTransaction = {
    'from': account,
     // transaction counter for account
    'nonce': '0x' + count.toString(16),
    'gasPrice': '0x003B9ACA00',
    'gasLimit': '0x250CA',
    // we are sending the transaction to the contract (not the destination account)
    'to': contractAddress,
    // the value of Ether sent is zero
    'value': '0x0',
    // this encodes the ABI of the method and the arguements
    'data': contract.methods.transfer(req.params.address, 1).encodeABI(),
    // ropsten chain id
    'chainId': 0x03
  }

  //the auto signing process with private key
  const privateKey = '4F912A80D6A6D7883E04BE1C08949C3827CE13A0FC293A2F3ACCAFEB65312E54'
  const signedTx = await web3.eth.accounts.signTransaction(rawTransaction, privateKey)
  console.log('signedTx', signedTx)
  const receipt = await web3.eth.sendSignedTransaction(signedTx.rawTransaction)

  res.json({ receipt: receipt })

})

const server = app.listen(4000)
server.setTimeout(90000)
console.log('I\'m running. Go to http://localhost:4000/ and check...')