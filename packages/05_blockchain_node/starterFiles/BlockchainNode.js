const express = require('express')
const bodyParser = require('body-parser')
const Blockchain = require('../Blockchain')
const rp = require('request-promise')

class BlockchainNode {
  constructor ({ host, port, address = 'emperor' }) {
    this.host = host
    this.port = port
    this.address = address
    this.nodeUrl = `http://${host}:${port}`
    this.networkNodes = []
    this.blockReward = 10
  }
}

/**
 * Factory method for starting blockchain nodes
 * @param host - host (IP address)
 * @param port - port
 * @param address - beneficient of successful mining
 * @returns {{app: *, node: BlockchainNode}}
 */
function start ({ host = 'localhost', port = 3000, address = 'emperor' } = {}) {
  const app = express()
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: false }))

  const node = new BlockchainNode({ host, port, address })
  const blockchain = new Blockchain()

  /**
   * Simple ping method
   */
  app.get('/', (req, res) => {
    res.send('I am OK')
  })

  /**
   * Returns serialized blockchain
   */
  app.get('/blockchain', (req, res) => {
    res.send(blockchain)
  })

  /**
   * Returns serialized node data
   */
  app.get('/node', (req, res) => {
    res.send(node)
  })

  /**
   * Gets balance by address
   */
  app.get('/balance/:address', (req, res) => {
      const { address } = req.params
      const balance = blockchain.getBalance(address)
      res.send({ address, balance })
    }
  )

  /**
   * Receives a transaction and broadcasts it to every node registered
   */
  app.post('/transaction', (req, res) => {
    const { amount, sender, recipient } = req.body
    const newTransaction = Blockchain.createNewTransaction({ amount, sender, recipient })
    blockchain.addTransaction(newTransaction)
    const requests = []
    node.networkNodes.forEach(networkNodeUrl => {
      const requestOptions = {
        uri: networkNodeUrl + '/transaction/receive',
        method: 'POST',
        body: newTransaction,
        json: true
      }
      requests.push(rp(requestOptions))
    })
    Promise.all(requests).then(data => {
      res.json({ note: 'Transaction created and broadcast sucessfully.' })
    })
  })

  /**
   * Adds new transaction to local blockchain. Endpoint for node to node communication.
   */
  app.post('/transaction/receive', (req, res) => {
    const newTransaction = req.body
    const blockIndex = blockchain.addTransaction(newTransaction)
    res.json({
      node: `Transaction will be added in block ${blockIndex}.`
    })
  })

  /**
   * Mines new block and broadcasts it in the network. Adds reward transaction if block is mined on this node.
   */
  app.get('/block/mine', (req, res) => {
    const newBlock = blockchain.mine()
    const requests = []
    node.networkNodes.forEach(networkNodeUrl => {
      const requestOptions = {
        uri: networkNodeUrl + '/block/receive',
        method: 'POST',
        body: { newBlock },
        json: true
      }
      requests.push(rp(requestOptions))
    })
    Promise.all(requests)
      .then(data => {
          const requestOptions = {
            uri: node.nodeUrl + '/transaction',
            method: 'POST',
            body: {
              amount: node.blockReward,
              sender: '00',
              recipient: node.address
            },
            json: true
          }
          return rp(requestOptions)
        }
      )
      .then(data => {
        res.json({
          note: 'New block mined & broadcast successfully',
          block: newBlock
        })
      })
  })

  /**
   * Adds new block to local blockchain and validates it. Endpoint for node to node communication.
   */
  app.post('/block/receive', (req, res) => {
    const { newBlock } = req.body
    const lastBlock = blockchain.getLastBlock()
    const correctHash = lastBlock.hash === newBlock.previousBlockHash
    const correctIndex = lastBlock['index'] + 1 === newBlock['index']
    if (correctHash && correctIndex) {
      blockchain.pushNewBlock(newBlock)
      blockchain.clearTransactions()
      console.log('newBlock accepted')
      res.json({ note: 'New block received and accepted', newBlock })
    } else {
      console.log('newBlock rejected')
      res.json({ note: 'New block rejected', newBlock })
    }
  })

  app.post('/register-and-broadcast-node', (req, res) => {
    const { newNodeUrl } = req.body.newNodeUrl
    if (node.networkNodes.indexOf(newNodeUrl) === -1) {
      node.networkNodes.push(newNodeUrl)
    }
    const reqNodesPromises = []
    node.networkNodes.forEach(networkNodeUrl => {
      const requestOptions = {
        uri: networkNodeUrl + '/register-node',
        method: 'POST',
        body: { newNodeUrl: newNodeUrl },
        json: true
      }
      reqNodesPromises.push(rp(requestOptions))
    })
    Promise.all(reqNodesPromises)
      .then(data => {
        // use the data
        const bulkRegisterOptions = {
          uri: newNodeUrl + '/register-nodes-bulk',
          method: 'POST',
          body: { allNetworkNodes: [...node.networkNodes, node.nodeUrl] },
          json: true
        }
        return rp(bulkRegisterOptions)
      })
      .then(data => {
        res.json({ note: 'New node registered with network successfully' })
      })
  })

  app.post('/register-node', (req, res) => {
    const newNodeUrl = req.body.newNodeUrl
    if (node.networkNodes.indexOf(newNodeUrl) === -1 && node.nodeUrl !== newNodeUrl) {
      node.networkNodes.push(newNodeUrl)
    }
    res.json({ note: 'New node registered successfully.' })
  })

  /**
   * Registers a bulk of nodeUrls
   */
  app.post('/register-nodes-bulk', (req, res) => {
    const allNetworkNodes = req.body.allNetworkNodes
    allNetworkNodes.forEach(networkNodeUrl => {
      if (node.networkNodes.indexOf(networkNodeUrl) === -1 && node.nodeUrl !== networkNodeUrl) {
        node.networkNodes.push(networkNodeUrl)
      }
    })
    res.json({ note: 'Bulk registration successful' })
  })

  app.get('/consensus', function (req, res) {
    const requestPromises = []
    node.networkNodes.forEach(networkNodeUrl => {
      const requestOptions = {
        uri: networkNodeUrl + '/blockchain',
        method: 'GET',
        json: true
      }
      requestPromises.push(rp(requestOptions))
    })
    Promise.all(requestPromises)
      .then(blockchains => {
        const currentChainLength = blockchain.chain.length
        let maxChainLength = currentChainLength
        let newLongestChain = null
        let newPendingTransactions = null
        blockchains.forEach(blockchain => {
          if (blockchain.chain.length > maxChainLength) {
            maxChainLength = blockchain.chain.length
            newLongestChain = blockchain.chain
            newPendingTransactions = blockchain.pendingTransactions
          }
        })
        if (!newLongestChain || (newLongestChain && !blockchain.chainIsValid(newLongestChain))) {
          res.json({
            note: 'Current chain has not been replaced.',
            chain: blockchain.chain
          })
        }
        else {
          blockchain.chain = newLongestChain
          blockchain.pendingTransactions = newPendingTransactions
          res.json({
            note: 'This chain has been replaced.',
            chain: blockchain.chain
          })
        }
      })
  })

  app.listen(node.port)

  return { app, node }
}

module.exports = start