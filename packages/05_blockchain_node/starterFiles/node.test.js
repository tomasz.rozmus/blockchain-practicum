const GENESIS_BLOCK = require('../Blockchain').GENESIS_BLOCK

const regenerateHex = require('utils/hex').regenerateHex

const request = require('supertest')
const start = require('./BlockchainNode')

const { app, node } = start({ port: 4000 })

describe('node', () => {
  // beforeAll(() => {
  //   app.close()
  // })
  it('express works', async () => {
    const response = await request(app)
      .get('/')
      .expect(200)

    expect(response.text).toEqual('I am OK')
  })

  it('inits with correct blockchain', async () => {
    const response = await request(app)
      .get('/blockchain')
      .expect(200)
    const { chain, pendingTransactions, difficulty } = response.body

    expect(chain.length).toEqual(1)
    expect(chain[0]).toEqual(GENESIS_BLOCK)
    expect(pendingTransactions).toEqual([])
    expect(difficulty).toEqual(regenerateHex('00ff'))
  })

  it('mines and gets balance', async () => {
    // We must mine 2 times to include 1 reward-transaction in block
    const r1 = await request(app)
      .get('/block/mine')
      .expect(200)

    const r2 = await request(app)
      .get('/block/mine')
      .expect(200)

    const b1 = await request(app)
      .get('/balance/emperor')
      .expect(200)

    expect(b1.body.balance).toEqual(10)
  })

})