const Blockchain = require('./Blockchain')
const GENESIS_BLOCK = require('./Blockchain').GENESIS_BLOCK

describe('BlockchainMining', () => {

  it('has valid initial structure', () => {
    const blockchain = new Blockchain()

    expect(blockchain.chain).toEqual([GENESIS_BLOCK])
    expect(blockchain.pendingTransactions).toEqual([])
    expect(blockchain.difficulty).toEqual('00ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff')
  })

  it('creates a transaction', () => {
    const amount = 100
    const from = 'foo'
    const to = 'bar'
    const transaction = Blockchain.createNewTransaction({ amount, sender: from, recipient: to })

    expect(transaction.amount).toEqual(100)
    expect(transaction.sender).toEqual('foo')
    expect(transaction.recipient).toEqual('bar')
    expect(transaction.transactionId).toEqual(expect.stringMatching(/^[0-9a-f]{32}$/i))
  })

  it('adds new transactions to pending transactions', () => {
    const blockchain = new Blockchain()
    const t1 = Blockchain.createNewTransaction({ amount: 100, sender: 'foo', recipient: 'bar' })
    const t2 = Blockchain.createNewTransaction({ amount: 40, sender: 'bar', recipient: 'baz' })

    blockchain.addTransaction(t1)
    blockchain.addTransaction(t2)

    expect(blockchain.pendingTransactions.length).toEqual(2)
    expect(blockchain.pendingTransactions[0]).toEqual(t1)
    expect(blockchain.pendingTransactions[1]).toEqual(t2)
  })

  it('creates a block', () => {
    const index = 1
    const transactions = []
    const nonce = 100
    const previousBlockHash = '0'
    const hash = '0'

    const block = Blockchain.createNewBlock({ index, transactions, nonce, previousBlockHash, hash })

    expect(block.index).toEqual(1)
    expect(block.nonce).toEqual(100)
    expect(block.transactions).toEqual([])
    expect(block.previousBlockHash).toEqual('0')
    expect(block.hash).toEqual('0')

  })

  it('adds genesis block if present as argument in constructor', () => {
    const genesisBlock = Blockchain.createNewBlock(GENESIS_BLOCK)
    const blockchain = new Blockchain({ genesisBlock })

    expect(blockchain.chain.length).toEqual(1)
    const block1 = blockchain.chain[0]
    expect(block1.index).toEqual(1)
    expect(block1.nonce).toEqual(100)
    expect(block1.transactions).toEqual([])
    expect(block1.previousBlockHash).toEqual('0')
    expect(block1.hash).toEqual('0')
  })

  it('gets last block', () => {
    const genesisBlock = Blockchain.createNewBlock(GENESIS_BLOCK)
    const blockchain = new Blockchain({ genesisBlock })

    const block1 = blockchain.getLastBlock()

    expect(block1).toEqual(genesisBlock)

    blockchain.addTransaction(Blockchain.createNewTransaction({ amount: 100, sender: 'foo', recipient: 'bar' }))
    blockchain.addTransaction(Blockchain.createNewTransaction({ amount: 40, sender: 'bar', recipient: 'baz' }))
    blockchain.mine()

    const block2 = blockchain.getLastBlock()

    expect(block2.transactions.length).toEqual(2)
  })

  it('performs proof of wWork', () => {
    const genesisBlock = Blockchain.createNewBlock(GENESIS_BLOCK)
    const blockchain = new Blockchain(genesisBlock)

    expect(blockchain.difficulty).toEqual('00ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff')

    const t1 = Blockchain.createNewTransaction({ amount: 100, sender: 'foo', recipient: 'bar' })
    const t2 = Blockchain.createNewTransaction({ amount: 40, sender: 'bar', recipient: 'baz' })
    const blockData = {
      index: 1,
      transactions: [t1, t2]
    }
    const nonce = blockchain.proofOfWork('0', blockData)

    expect(typeof nonce).toEqual('number')
  })

  it('mines', () => {
    const genesisBlock = Blockchain.createNewBlock(GENESIS_BLOCK)
    const blockchain = new Blockchain({ genesisBlock })

    expect(blockchain.chain.length).toEqual(1)

    blockchain.addTransaction(Blockchain.createNewTransaction({ amount: 100, sender: 'foo', recipient: 'bar' }))
    blockchain.addTransaction(Blockchain.createNewTransaction({ amount: 40, sender: 'bar', recipient: 'baz' }))

    expect(blockchain.pendingTransactions.length).toEqual(2)

    const minedBlock1 = blockchain.mine()

    expect(blockchain.pendingTransactions.length).toEqual(0)
    expect(blockchain.chain.length).toEqual(2)
    expect(blockchain.getLastBlock()).toEqual(minedBlock1)
    expect(blockchain.chain[0].hash).toEqual(blockchain.chain[1].previousBlockHash)

    blockchain.addTransaction(Blockchain.createNewTransaction({ amount: 10, sender: 'baz', recipient: 'bar' }))

    expect(blockchain.pendingTransactions.length).toEqual(1)

    const minedBlock2 = blockchain.mine()

    expect(blockchain.pendingTransactions.length).toEqual(0)
    expect(blockchain.chain.length).toEqual(3)
    expect(blockchain.getLastBlock()).toEqual(minedBlock2)
    expect(blockchain.chain[1].hash).toEqual(blockchain.chain[2].previousBlockHash)

    const valid = Blockchain.isValidChain(blockchain.chain)

    expect(valid).toEqual(true)
  })

})