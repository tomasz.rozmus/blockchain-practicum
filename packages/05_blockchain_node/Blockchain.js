const { uniqueId } = require('utils/uniqueId')
const pow = require('utils/proofOfWork')
const fulfills = require('utils/fulfills')
const sha256 = require('utils/sha256')

const INITIAL_DIFFICULTY = '00ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff'

const GENESIS_BLOCK = {
  index: 1,
  transactions: [],
  nonce: 100,
  previousBlockHash: '0',
  hash: '0',
  timestamp: '0'
}

class Blockchain {

  /**
   * Creates a blockchain from existing chain or with genesisBlock provided. Throws error if chain is invalid.
   * @param chain
   * @param difficulty
   * @param genesisBlock
   */
  constructor ({ chain = [], difficulty = INITIAL_DIFFICULTY, genesisBlock = GENESIS_BLOCK } = {}) {
    this.chain = chain
    this.difficulty = difficulty
    this.pendingTransactions = []
    if (genesisBlock !== null && this.chain.length === 0) {
      this.pushNewBlock(genesisBlock)
    }
    if (!Blockchain.isValidChain(this.chain)) {
      throw new Error('Invalid chain!')
    }

  }

  static createNewTransaction ({ amount, sender, recipient }) {
    return {
      amount,
      sender,
      recipient,
      transactionId: uniqueId()
    }
  }

  static createNewBlock ({ index, timestamp = Date.now(), transactions, nonce, previousBlockHash, hash }) {
    return {
      index,
      timestamp,
      transactions,
      nonce,
      hash,
      previousBlockHash
    }
  }

  /**
   * Hashing function used to produce block hash.
   * Hashed string is constructed the same way as in proofOfWork algorithm (previousHash + data + nonce),
   * also, the same alghoritm is used (sha256 from utils package)
   * @param {string} previousBlockHash - previous block hash
   * @param blockData - block data to be stringified
   * @param nonce - nonce
   * @returns {string}
   */
  static hashBlock ({ previousBlockHash, blockData, nonce }) {
    return sha256(previousBlockHash + JSON.stringify(blockData) + nonce)
  }

  /**
   * Validates given chain up to the genesis block
   * @param chain
   * @returns {boolean}
   */
  static isValidChain (chain) {
    let validChain = true

    for (let i = 1; i < chain.length; i++) {
      const currentBlock = chain[i]
      const prevBlock = chain[i - 1]
      const blockHash = Blockchain.hashBlock(
        {
          previousBlockHash: prevBlock['hash'],
          blockData: {
            transactions: currentBlock['transactions'],
            index: currentBlock['index']
          },
          nonce: currentBlock['nonce']
        })
      if (!fulfills(blockHash, INITIAL_DIFFICULTY)) validChain = false
      if (currentBlock['previousBlockHash'] !== prevBlock['hash']) validChain = false
    }

    const genesisBlock = chain[0]
    const correctNonce = genesisBlock['nonce'] === 100
    const correctPreviousBlockHash = genesisBlock['previousBlockHash'] === '0'
    const correctHash = genesisBlock['hash'] === '0'
    const correctTransactions = genesisBlock['transactions'].length === 0

    if (!correctNonce || !correctPreviousBlockHash || !correctHash || !correctTransactions) validChain = false

    return validChain
  }

  /**
   * Adds transaction to pending transactions and returns index of block which will contain it.
   * @param transaction
   * @returns {number}
   */
  addTransaction (transaction) {
    this.pendingTransactions.push(transaction)
    return this.getLastBlock().index + 1
  }

  pushNewBlock (block) {
    this.chain.push(block)
  }

  clearTransactions () {
    this.pendingTransactions = []
  }

  getLastBlock () {
    return this.chain[this.chain.length - 1]
  }

  proofOfWork (previousBlockHash, blockData) {
    const dataAsString = previousBlockHash + JSON.stringify(blockData)
    return pow(dataAsString, this.difficulty)
  }

  /**
   * Gets balance based on existing blocks
   * @param address
   * @returns {T | number}
   */
  getBalance (address) {
    return this.chain.reduce((blockAccumulator, block) => {
      return blockAccumulator + block.transactions.reduce((transactionAccumulator, transaction) => {
        if (transaction.recipient === address) {
          return transactionAccumulator += transaction.amount
        }
        else if (transaction.sender === address) {
          return transactionAccumulator -= transaction.amount
        }
        return 0
      }, 0)
    }, 0)
  }

  /**
   * Mines new block from pending transactions. Returns newly mined block.
   * @returns {{index, timestamp, transactions, nonce, hash, previousBlockHash}|*}
   */
  mine () {
    const lastBlock = this.getLastBlock()
    const previousBlockHash = lastBlock.hash
    const blockData = {
      transactions: this.pendingTransactions,
      index: lastBlock.index + 1
    }
    const nonce = this.proofOfWork(previousBlockHash, blockData)
    const currentBlockHash = Blockchain.hashBlock({ previousBlockHash, blockData, nonce })

    const newBlock = Blockchain.createNewBlock({
      index: blockData.index,
      transactions: blockData.transactions,
      nonce,
      previousBlockHash,
      hash: currentBlockHash
    })
    this.pushNewBlock(newBlock)
    this.clearTransactions()

    return newBlock
  }

}

module.exports = Blockchain
module.exports.GENESIS_BLOCK = GENESIS_BLOCK