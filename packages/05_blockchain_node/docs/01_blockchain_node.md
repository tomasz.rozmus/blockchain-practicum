# The Blockchain - adding network node
To bring our blockchain online, we need some kind of software which provides network endpoints for some features - a server.
Most popular server library in node.js is called express. It's a wrapper around node.js' build-in mechanisms (`http`) , which provides more advanced features with minimal effort.
https://www.npmjs.com/package/express
We will use express to build public api for the blockchain.

Type `yarn` in project root to install missing packages and type `yarn server` inside this package. Navigate to http://localhost:3000 and see how simple it is to create a server with `express`.

Exercise:
Install `postman`. Execute `yarn start`. Check in the browser if http://localhost:3000/ is working. Then go to postman and create a method for adding new transaction. Add few transactions and check blockchain status with http://localhost:3000/blockchain

Automatic testing of whole servers is advanced concept, but worth mentioning. There is utility called `supertest` which is used for testing `express` servers. 
https://www.npmjs.com/package/supertest

Take a look at `node.test.js`, run it with `yarn test-watch`

Examine how coin supply is created in `/block/mine` method of networkNode. That will be the only way of creating value in our blockchain.
(Presentation of working blockchain)

Before exercises:
Start by copying `starterFiles/*` to `execrcises/*`. 

Exercise:
Based on current `node.test.js` and `BlockchainNode.js` example, create an endpoint, a test and a postman method for checking balances. Use `Blockchain.getBalance()`. 

```
app.get('/balance/:address', (req, res) => {
    const {address} = req.params
    // ... code goes here   
    res.send({address, balance}
  }
)

```

Exercise:
Extend `Blockchain.js` and `BlockchainNode.js` to show all balances in a blockchain (based on transaction history).

```
app.get('/balances', (req, res) => {
    // ... code goes here   
  }
)

```

Exercise:
Extend `Blockchain.js` to validate if user has funds before adding a transaction to pending transactions.

