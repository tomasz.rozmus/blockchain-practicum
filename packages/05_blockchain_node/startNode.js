const start = require('./starterFiles/BlockchainNode') // change to ./exercises/BlockchainNode while developing

const port = process.argv[2] || 3000
const address = process.argv[3] || 'miner1'
const host = process.argv[4] || 'localhost'

function main () {
  const { node } = start({
    host,
    port,
    address
  })

  console.log('Blockchain node started with: ')
  console.log(node)
}

main()