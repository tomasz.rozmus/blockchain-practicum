const sha256 = require('utils/sha256')

const MAX_ITERATIONS = Math.pow(10, 8)

function proofOfWorkSimple (data, difficulty) {
  for (let nonce = 0; nonce <= MAX_ITERATIONS; nonce++) {
    const hash = sha256(data + nonce)
    if (hash.indexOf(difficulty) === 0) { // hash string starts from difficulty string (fulfills difficulty)
      return nonce
    }
  }
  throw Error('Maximum iterations reached')
}

module.exports = proofOfWorkSimple