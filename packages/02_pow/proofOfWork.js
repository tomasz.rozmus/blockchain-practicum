const { sha256, parseBigInt } = require('utils')

const MAX_ITERATIONS = Math.pow(10, 7)

function fulfills (hash, difficulty) {
  const hashAsNumber = parseBigInt(hash, 16)
  const difficultyAsNumber = parseBigInt(difficulty, 16)
  return hashAsNumber - difficultyAsNumber < BigInt(0)
}

function proofOfWork (data, difficulty) {
  for (let i = 0; i <= MAX_ITERATIONS; i++) {
    const nonce = Math.floor(Math.random() * MAX_ITERATIONS)
    const hash = sha256(data + nonce)
    if (fulfills(hash, difficulty)) { // hash is lower than difficulty
      return nonce
    }
  }
  throw Error('Maximum iterations reached')
}

module.exports = proofOfWork