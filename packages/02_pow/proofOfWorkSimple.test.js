const proofOfWork = require('./proofOfWorkSimple')

const data = Object.freeze({
  transactions: [
    {
      from: 'a',
      to: 'b',
      amount: '10'
    },
    {
      from: 'b',
      to: 'c',
      amount: '5'
    }
  ]
})

const preparedData = JSON.stringify(data)

describe('proofOfWorkSimple', () => {
  it('works for difficulty 2', () => {
    const difficulty = '00'
    const nonce = proofOfWork(preparedData, difficulty)
    console.log('00', nonce)
    expect(typeof nonce).toEqual('number')
  })

  it('works for difficulty 3', () => {
    const difficulty = '000'
    const nonce = proofOfWork(preparedData, difficulty)
    console.log('000', nonce)
    expect(typeof nonce).toEqual('number')
  })

  it('works for difficulty 4', () => {
    const difficulty = '0000'
    const nonce = proofOfWork(preparedData, difficulty)
    console.log('0000', nonce)
    expect(typeof nonce).toEqual('number')
  })

  it('works for difficulty 5', () => {
    const difficulty = '00000'
    const nonce = proofOfWork(preparedData, difficulty)
    console.log('00000', nonce)
    expect(typeof nonce).toEqual('number')
  })

  it.skip('works for difficulty 6', () => {
    const difficulty = '000000'
    const nonce = proofOfWork(preparedData, difficulty)
    console.log('000000', nonce)
    expect(typeof nonce).toEqual('number')
  })
})