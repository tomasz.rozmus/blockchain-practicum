const proofOfWork = require('./proofOfWork')

const data = Object.freeze({
  transactions: [
    {
      from: 'a',
      to: 'b',
      amount: '10'
    },
    {
      from: 'b',
      to: 'c',
      amount: '5'
    }
  ]
})

const preparedData = JSON.stringify(data)
//              realHash = 'ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad'
const easyDifficulty = '00ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff'
const normDifficulty = '008fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff'
const hardDifficulty = '001fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff'

describe('proofOfWork', () => {
  it('works for easy difficulty', () => {
    const nonce = proofOfWork(preparedData, easyDifficulty)
    console.log('easy', nonce)
    expect(typeof nonce).toEqual('number')
  })

  it('works for moderate difficulty', () => {
    const nonce = proofOfWork(preparedData, normDifficulty)
    console.log('moderate', nonce)
    expect(typeof nonce).toEqual('number')
  })

  it('works for hard difficulty', () => {
    const nonce = proofOfWork(preparedData, hardDifficulty)
    console.log('hard', nonce)
    expect(typeof nonce).toEqual('number')
  })
})