# Proof Of Work (POW)

[Basic info](./../../../stack/pow.md)

Exercise: create a `proofOfWorkSimple` function using TDD approach.
```
 function proofOfWorkSimple(data, difficulty){
    // ...
    return nonce
 }
```
data - any string data
difficulty - 'zeros' at the beginning of the hexadecimal hash, ex. difficulty '00' => 009123...  fulfils, '000' => 0004322...  fulfils.


