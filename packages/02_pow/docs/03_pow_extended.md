Let's get back to proofOfWork method. We need to make it more precise in case of difficulty. POW is widely used in many blockchain solutions, like Bitcoin and ethereum. POW was also considered as an universal SPAM protection mechanism.
https://en.wikipedia.org/wiki/Proof-of-work_system
https://www.investopedia.com/terms/p/proof-work.asp
 
Create a folder  `02_pow/exercises`, use this folder for exercises file. It's stated in `.gitignore`, so you wont have conflicts anymore.

Exercise: create a `proofOfWork` function. Tests are prepared.
```
 function proofOfWork(data, difficulty){
    // ...
    return nonce
 }
```
data - any string data  
difficulty - hexadecimal number which is a maximum hash value for a given data and nonce

In other words: hash of (data + nonce) should be lower than difficulty hex number.  
Use parseBigInt() function from utils. Read it's test to gain better overview of safe math operations.

Exercise: Copy previous solutions to `proofOfWorkRandom.*.js`. Change nonce guessing method to be random instead of incremental

Use Math.random()
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random

Compare the methods.
