
Exercise: create a `calibrateDifficulty` function which guarantees that POW function executes on average between `min` and `max` milliseconds in 90% cases.

```
function calibrateDifficulty(minMilliseconds, maxMilliseconds) {
    // ...
    return difficulty
}

```