# PoW performance

We need to test our `proofOfWork` algorithm to check how fast it handles finding a nonce. We will use `process.hrtime()`, more info [here](https://nodejs.org/dist/latest-v10.x/docs/api/process.html#process_process_hrtime_time)

Let's automate performance checks with https://nodemon.io/. `nodemon` watches for file changes and (re)executes them. It's similar to `jest --watch`, but runs any code, not only tests.

Exercise: create a `averageTimings` function which will print execution times for previously created proofOfWork with three different difficulty levels (0000ff, 00008f, 00004f), 10 probes for each difficulty. Use `nodemon`, add it to npm scripts under "timings", and "timings-watch".

```
function averageTimings() {
    // ...
}

```
