We need better organisation in our projects. Yarn is another package manager for JS which has some features we need.

[More about yarn](./../../../stack/yarn.md)

https://yarnpkg.com

Install it:
https://yarnpkg.com/lang/en/docs/install/#debian-stable

Grasp basic differences here:
https://shift.infinite.red/npm-vs-yarn-cheat-sheet-8755b092e5cc
https://infinite.red/files/yarn.pdf

We will use workspaces feature to save space and simplify our projects/dependencies.

[More about yarn workspaces](./../../../stack/workspaces.md)

After installing `yarn`, move all your local changes to separate branch and fetch `master`. If you have local commits, move them to another branch (you can use `git reset --soft <commit>`, ..., `git branch -m feature/my-local-work`)

Now we have our classes-directories inside `/packages` folder. There is also `/packages/utils` folder for commonly used features. This folder will be growing with new features overtime.

Remove all your `node_modules` manually (they are ingored so resetting branch wont help).

Having the most recent master, in the root folder of the project, type `yarn`. This will install all dependencies in a more optimized way.

From now on we will be calling our project a `monorepo` and respective classes will be `packages` of this monorepo. Each package is a kind of subproject, and can link to others packages resources in an easy way.

 



