# Hash functions

[Basic info](./../../../stack/hash.md)

# SHA256
`sha256('abc') => 'ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad'`

We use 256 bit word as a hash result. That translates into 64 digit hex number.
`1234567890123456789012345678901234567890123456789012345678901234` 
`ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff`

4 bits per hex digit
hex `f` => binary `1111`
64 digits/chars => 4 * 64 = 256 bits

Exercise: create a `merkleTree` function which generates hash

```

function merkleTreeHashSimple(data) {
    // ...
    return treeHash
}

```
data is an array of strings