# Local Smart contract development workflow

## Web3
Web3.js is a collection of libraries which allows you to interact with a local or remote Ethereum node, using HTTP, WebSocket or IPC connection.

## Infura
Infura is easy to use API and developer tools, providing reliable and scalable access to Ethereum and IPFS. It provides access to ethereum node endpoints.
In order to use it, you must register there and obtain api key.

Exercise: Register in Infura, get your ApiKey and save it. Switch MetaMask to use your infura api endpoint for ropsten network.

## Ganache
The Ganache is a Node.js Ethereum client for testing and developing smart contracts.

## Truffle
Truffle suite is a set of tools helpful in Smart Contract development and testing.

## Solc
Solc.js is node.js Ethereum based compiler.
 
## Contract Application Binary Interface - ABI
The ABI, Application Binary Interface, is basically how you call functions in a contract and get data back. It's a metadata describing contract interface.
