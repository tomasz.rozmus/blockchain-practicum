# Compilation

We can use `solc` compiler to transform Solidity contract code into deployable bytecode.

Type: `yarn compile-solc` and examine `build/contracts/InboxSolc.json` file to see the structure.

Truffle is a suite which internally uses Solc to compile contracts.

Type `yarn compile-truffle`, examine `Inbox.json` and compare it to the InboxSolc.json.

There are differences in json structure, but bytecode is the same.

Having contract compiled, we can use its ABI to construct an accessible contract instance in programming language of our choice.
In javascript we can do it by using web3.

Take a look at `test/Inbox.test.js` to see how you can use web3 and a virtual local blockchain to test your contracts.

You can also test you contract on the online blockchain (testnets or real one). Take a look at `test/InboxInfura.test.js`.

Exercise:
Create a test case for your previous Faucet.sol code. Copy it to `/contracts`, run `compile-truffle` script and create `Faucet.test.js` script relying on the `Inbox.test.js` structure. Validate it by running `yarn test-faucet`. Test suite should check:
* initial amounts for accounts
* contract address can be credited with ether from every address (amount before and after operation change)
* withdrawing less than 0.1 ether succeded
* withdrawing more produces an error (use try / catch)

Tip: remember that accounts[0] is contract deployer
