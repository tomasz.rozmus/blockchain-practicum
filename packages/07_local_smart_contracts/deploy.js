const HDWalletProvider = require('truffle-hdwallet-provider')
const Web3 = require('web3')
const { abi, bytecode } = require('./build/contracts/Inbox')

const provider = new HDWalletProvider(
  'river cloth outdoor result share peace dinner web humble foil celery tuition',
  'https://rinkeby.infura.io/v3/f92d225fec0c4154bc1c84391ef4574f'
)

const web3 = new Web3(provider)

const deploy = async () => {
  const accounts = await web3.eth.getAccounts()

  console.log('Attempting to deploy from account', accounts[0])

  console.log(accounts)
  const result = await new web3.eth.Contract(JSON.parse(abi))
    .deploy({ data: bytecode, arguments: ['Hi there!'] })
    .send({ gas: '1000000', from: accounts[0] })

  console.log('Contract deployed to', result.options.address)
}

deploy().catch(console.error) // 0x1A3F50AFc15927CEaB51D04EB0bDFfb76E03366f