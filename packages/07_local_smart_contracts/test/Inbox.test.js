const ganache = require('ganache-cli')
const Web3 = require('web3')
const provider = ganache.provider()
provider.setMaxListeners(15);
const web3 = new Web3(provider) // here we provide our virtual, ganache provider.
const contract = require('../build/contracts/Inbox')
const contractInterface = contract['abi']
const contractBytecode = contract['bytecode']

const INITIAL_STRING = 'Hi there!'

let accounts
let inbox

beforeEach(async () => {
  // Get a list of all accounts
  accounts = await web3.eth.getAccounts()
  // console.log('accounts', accounts)

  // The Contract
  // create contract instance based on Application Binary Interface (ABI)
  const contractInstance = new web3.eth.Contract(contractInterface)
  // deploy the contract passing initial value to constructor
  const contractDeployed = contractInstance.deploy({
    data: contractBytecode,
    arguments: [INITIAL_STRING]
  })
  // Use the first account to deploy
  inbox = await contractDeployed.send({ from: accounts[0], gas: '5000000' })
})

describe('Inbox', () => {

  it('has accounts', async () => {
    expect(accounts.length).toEqual(10)
    const balance0 = await web3.eth.getBalance(accounts[0])
    expect(balance0).toEqual("99999377242000000000")
  })

  it('deploys a contract witch proper methods', () => {
    expect(inbox.methods).toMatchSnapshot()
  })

  it('has a default message', async () => {
    const message = await inbox.methods.message().call()
    expect(message).toEqual(INITIAL_STRING)
  })

  it('contract has zero balance', async () => {
    const balance = await web3.eth.getBalance(inbox.options.address)

    expect(balance).toEqual("0")
  })

  it('can change the message', async () => {
    await inbox.methods.setMessage('bye').send({
      from: accounts[0]
    })
    const message = await inbox.methods.message().call()
    expect(message).toEqual('bye')
  })

})