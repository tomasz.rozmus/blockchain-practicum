const ganache = require('ganache-cli')
const Web3 = require('web3')
const provider = ganache.provider()
provider.setMaxListeners(15);
const web3 = new Web3(provider) // here we provide our virtual, ganache provider.
const contract = require('../build/contracts/Faucet')
const contractInterface = contract['abi']
const contractBytecode = contract['bytecode']

const INITIAL_STRING = 'Hi there!'

let accounts
let inbox

beforeEach(async () => {
  // Get a list of all accounts
  accounts = await web3.eth.getAccounts()
  // console.log('accounts', accounts)

  // The Contract
  // create contract instance based on Application Binary Interface (ABI)
  const contractInstance = new web3.eth.Contract(contractInterface)
  // deploy the contract passing initial value to constructor
  const contractDeployed = contractInstance.deploy({
    data: contractBytecode,
    arguments: [INITIAL_STRING]
  })
  // Use the first account to deploy
  inbox = await contractDeployed.send({ from: accounts[0], gas: '5000000' })
})

const oneEth =   "1000000000000000000";
const smallEth = "90000000000000000";

describe('Inbox', () => {

  it('has accounts', async () => {
    expect(accounts.length).toEqual(10)
    const balance0 = await web3.eth.getBalance(accounts[0])
    expect(balance0).not.toEqual("0")
  })

  it('deploys a contract witch proper methods', () => {
    expect(inbox.methods).toMatchSnapshot()
  })

  it('contract has zero balance', async () => {
    const balance = await web3.eth.getBalance(inbox.options.address)

    expect(balance).toEqual("0")
  })

  it('allows transfer to the contract', async () => {
    await web3.eth.sendTransaction({
      from: accounts[0],
      to: inbox.options.address,
      value: oneEth
    })
    const balance = await web3.eth.getBalance(inbox.options.address)

    expect(balance).toEqual(oneEth)
  })


  it('allows small withdrawals', async () => {
    await web3.eth.sendTransaction({
      from: accounts[0],
      to: inbox.options.address,
      value: oneEth
    })
    const contractBalanceBefore = await web3.eth.getBalance(inbox.options.address)
    const account1BalanceBefore = await web3.eth.getBalance(accounts[1])

    await inbox.methods.withdraw(smallEth).call({
      from: accounts[1]
    })

    const contractBalanceAfter = await web3.eth.getBalance(inbox.options.address)
    const account1BalanceAfter = await web3.eth.getBalance(accounts[1])

    expect(contractBalanceBefore).not.toEqual(contractBalanceAfter)
    expect(account1BalanceBefore).not.toEqual(account1BalanceAfter)
  })

})