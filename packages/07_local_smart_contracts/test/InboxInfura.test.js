const { delay } = require('utils')
const HDWalletProvider = require('truffle-hdwallet-provider')
const Web3 = require('web3')

const provider = new HDWalletProvider(
  'river cloth outdoor result share peace dinner web humble foil celery tuition',
  'https://ropsten.infura.io/v3/370de4d612ca47efaf482d0572656d6f'
)

const web3 = new Web3(provider)
const contract = require('../build/contracts/Inbox')
const contractInterface = contract['abi']

const INITIAL_STRING = 'Hi there!'

let accounts
let inbox

jest.setTimeout(100000)

beforeEach(async () => {
  // Get a list of all accounts
  accounts = await web3.eth.getAccounts()

  // The Contract
  // get contract instance based on Application Binary Interface (ABI) and already deployed address
  inbox = new web3.eth.Contract(contractInterface, '0x0db313d951d109b39727775816864341d0c8a824')
})

describe('InboxInfura', () => {

  it('has accounts', () => {
    expect(accounts).toMatchSnapshot()
  })

  it('contract has proper methods', () => {
    expect(inbox.methods).toMatchSnapshot()
  })

  it.skip('has any message', async () => {
    const message = await inbox.methods.message().call()
    expect(message.length).toBeGreaterThan(0)
  })

  it('can change the message', async () => {
    const newMessage = 'bye ' + Math.random()
    await inbox.methods.setMessage(newMessage).send({
      from: accounts[0],
      gas: 100000
    })
    await delay(33)
    const message = await inbox.methods.message().call()
    expect(message).toEqual(newMessage)
  })

})