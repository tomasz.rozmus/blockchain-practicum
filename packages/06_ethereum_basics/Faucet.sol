pragma solidity ^0.4.19;

contract Faucet {

    // function for withdrawing ether by the sender
    function withdraw(uint withdraw_amount) public {
        // Limit withdrawal to 0.1 ether
        require(withdraw_amount <= 100000000000000000);

        msg.sender.transfer(withdraw_amount);
    }

    // default fallback function
    function () public payable { }

}