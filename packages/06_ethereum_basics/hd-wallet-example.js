const hdkey = require('ethereumjs-wallet/hdkey')
const bip39 = require('bip39')

// You can generate a mnemonic seed with bip39 library
const mnemonic = 'one two three hour file six seven eight fine ten clever twelve'
const examplePath = `m/44'/60'/0'/0` // for 0 account
const getPath = ({ purpose = 44, coin = 60, account = 0 }) => `m/${purpose}'/${coin}'/0'/0/${account}`

const seed = bip39.mnemonicToSeedSync(mnemonic)
const hdwallet = hdkey.fromMasterSeed(seed)

// const address = '0x' + wallet.getAddress().toString('hex')

for (let i = 0; i < 20; i++) {
  const wallet = hdwallet.derivePath(getPath({ account: i })).getWallet()
  const address = '0x' + wallet.getAddress(i).toString('hex')
  console.log(getPath({account: i}), ' : ', address)
}