# Metamask
MetaMask is a bridge that allows you to visit the distributed web of tomorrow in your browser today. It allows you to run Ethereum dApps right in your browser without running a full Ethereum node. (metamask.io)
https://metamask.io/
https://medium.com/@mail.bahurudeen/setup-a-metamask-ethereum-wallet-and-use-it-to-send-and-receive-ether-4f3b99360e4f

Hold on with mnemonic (vault) part.
https://bitcoin.stackexchange.com/questions/74142/how-do-hd-wallets-use-mnemonic-to-recover-all-private-keys
Run `yarn bip39-example`

Exercise: Use the same words from example and try to recreate addresses on this website https://iancoleman.io/bip39/

## Test networks
Test networks are ethereum blockchains without real money and usually with simpler proof of work mechanism to provide cheaper operational costs. Most known networks are:
* Ropsten
* Rinkeby
* Kovan
* Goerli

A faucet is a place where you can obtain free test ether (for test networks).

Exercise: Go to https://faucet.ropsten.be/ and request some test ether for your main account address. Save the transaction hash and check it in ropsten.etherscan.io.

Exercise:
Transfer 0.1 ETH to your second account.

Exercise:
Transfer 0.05 ETH to colleague on the right.

0xdf88bc963E614FAB2bda81c298056ba18e01A424
