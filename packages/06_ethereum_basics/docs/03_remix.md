# Remix
Remix is online IDE for Solidity and Smart Contracts.

Let's start using it in practise. We will deploy a faucet very similar to one we used previously.

Go to remix.ethereum.org, create new file, name it Faucet.sol
Log in to Metamask, switch to Ropsten test network. Copy `Faucet.sol` contents (from this project) into the new contract.
Select exactly the same solidity version as in pragma in Compile tab. Switch to Run, click Deploy. 
Copy newly deployed contract address and check it in etherscan.

Exercise: Send 0.9 ether to the contract via metamask (use its address as a recipient).

Check transaction log for this contract one more time to ensure funding is done.

Go back to remix, click the withdraw function. Withdraw 0.01 ether, which is `10000000000000000` Wei.
https://www.etherchain.org/tools/unitConverter

Then try to withdraw 0.2 Ether. 

Exercise: Switch to Solidity 0.4.25. Extend and redeploy a contract which allows withdrawing all the ether if owner calls withdraw function.
Tip: Check the first code example here https://solidity.readthedocs.io/en/v0.4.25/contracts.html?highlight=contracts#creating-contracts

Exercise: Add a functionality which ensures that each address except the owner can withdraw in total only 0.05 Ether.
Tip: Use mappings: https://solidity.readthedocs.io/en/v0.4.25/types.html?highlight=mappings#mappings
  





