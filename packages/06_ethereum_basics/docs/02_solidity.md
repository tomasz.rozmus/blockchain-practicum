 # Solidity
 Solidity is an object-oriented, high-level language for implementing smart contracts. Smart contracts are programs which govern the behaviour of accounts within the Ethereum state. (https://solidity.readthedocs.io)
 
 Other languages:
 * Vyper
 * Flint
 * LLLL
 
 I encourage you to get through the documentation in https://solidity.readthedocs.io after gaining some basic experience.