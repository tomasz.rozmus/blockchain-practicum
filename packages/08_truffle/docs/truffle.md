# Truffle Suite
A world class development environment, testing framework and asset pipeline for blockchains using the Ethereum Virtual Machine (EVM), aiming to make life as a developer easier. With Truffle, you get:

* Built-in smart contract compilation, linking, deployment and binary management.
* Automated contract testing for rapid development.
* Scriptable, extensible deployment & migrations framework.
* Network management for deploying to any number of public & private networks.
* Package management with EthPM & NPM, using the ERC190 standard.
* Interactive console for direct contract communication.
* Configurable build pipeline with support for tight integration.
* External script runner that executes scripts within a Truffle environment.

This example was created with `truffle unbox metacoin` command - a scaffoling project to demonstrate its basic concepts. Directories in the project are organized like this:
* `contracts` - for contracts solidity code
* `migrations` - for deployment procedures (called migrations)
* `test` - for test files (can be written in .js or .sol)

There is also
* `truffle-config.js` - configuration file for network management

Truffle internally uses `solc` and `mocha`, you can see a list of dependencies here:
https://github.com/trufflesuite/truffle/blob/develop/packages/truffle/package.json

Execute: `truffle test ./test/metacoin.js` - this command will:
 * generate local blockchain (default build-in or ganache)
 * create accounts from random mnemonic 
 * compile contract to bytecode, ABI and other artifacts
 * inject artifacts and accounts in a javascript test runner (mocha)
 * run tests
 
As you see every test starts with `const metaCoinInstance = await MetaCoin.deployed();` - to ensure that contract is clean and without old data. 

Type `truffle develop`, this will create local blockchain listening on port 9545. Type `CTRL + C` to stop.

Type `truffle compile`. Then change method name from `sendCoin` to `sendCoins` and type `truffle compile` again - have bytecode changed?

Exercise: Copy MetaCoin.sol to MetaCoinGreeting.sol Extend it with `greetings` mapping which holds last message associated with receiver address. Add `sendCoinWithGreeting` and `getGreeting` methods and add tests for them. Start with .js tests, but writing tests in Solidity will be additionally rewarded.
Execute it with `truffle test ./test/metacoinGreeting.js`. 
Tip: Make sure to extend `2_deploy_contracts.js` with new contract migrations.

## Ganache

You can use truffle with ganache. Download ganache appfrom https://truffleframework.com/ganache
Make sure you marked 'allow executing file as a program' in ganache image permissions. Run Ganache with 'Quickstart' mode.

Uncomment `development` section in truffle-config.js. Go to contracts section in ganache and add your project to the workspace by pointing to `truffle-config.js`. Go to Settings->Advanced and turn on 'Verbose logs'.

Run `truffle migrate` and see what happens. Explore Accounts, Blocks, Contracts, Events and Logs tabs.

Type `truffle console` - it will open interactive REPL-like console with access to deployed contracts. Then type `networks` - it will show contract addresses.

Open MetaMask in your browser. Add custom RPC endpoint with `http://127.0.0.1:7545`, call it Ganache. Add Custom Token with MetaCoin address from previous command. You should see 0 in the amount.
Execute below script line after line (press enter after semicolon). Having Ganache opened, examine transactions.

```
var accounts = await web3.eth.getAccounts();
console.log(accounts);
var metacoin = await MetaCoin.deployed();
var receiver = '<your_main_account_address>';
var tx = await metacoin.sendCoin(receiver, 11);
console.log(tx);
var balance = await metacoin.getBalance.call(receiver);
console.log(balance.toString());
```

Exercise: Interact with deployed MetaCoinGreeting contract.
