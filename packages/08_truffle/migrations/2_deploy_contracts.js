const ConvertLib = artifacts.require("ConvertLib");
const MetaCoin = artifacts.require("MetaCoin");
// const MetaCoinGreeting = artifacts.require("MetaCoinGreeting");

module.exports = function(deployer) {
  deployer.deploy(ConvertLib);
  deployer.link(ConvertLib, MetaCoin);
  deployer.deploy(MetaCoin);
  //
  // deployer.deploy(ConvertLib);
  // deployer.link(ConvertLib, MetaCoinGreeting);
  // deployer.deploy(MetaCoinGreeting);
};
