const Blockchain = require('./BlockchainSimple')

describe('BlockchainSimple', () => {
  it('has valid initial structure', () => {
    const blockchain = new Blockchain()

    expect(blockchain.chain).toEqual([])
    expect(blockchain.pendingTransactions).toEqual([])
  })

  it('creates a transaction', () => {
    const amount = 100
    const from = 'foo'
    const to = 'bar'
    const transaction = Blockchain.createNewTransaction(amount, from, to)

    expect(transaction.amount).toEqual(100)
    expect(transaction.sender).toEqual('foo')
    expect(transaction.recipient).toEqual('bar')
    expect(transaction.transactionId).toEqual(expect.stringMatching(/^[0-9a-f]{32}$/i))
  })

  it('adds new transactions to pending transactions', () => {
    const blockchain = new Blockchain()
    const t1 = Blockchain.createNewTransaction(100, 'foo', 'bar')
    const t2 = Blockchain.createNewTransaction(40, 'bar', 'baz')

    blockchain.addTransaction(t1)
    blockchain.addTransaction(t2)

    expect(blockchain.pendingTransactions.length).toEqual(2)
    expect(blockchain.pendingTransactions[0]).toEqual(t1)
    expect(blockchain.pendingTransactions[1]).toEqual(t2)
  })

  it('creates a block', () => {
    const index = 1
    const transactions = []
    const nonce = 100
    const previousBlockHash = '0'
    const hash = '0'

    const block = Blockchain.createNewBlock(index, transactions, nonce, previousBlockHash, hash)

    expect(block.index).toEqual(1)
    expect(block.nonce).toEqual(100)
    expect(block.transactions).toEqual([])
    expect(block.previousBlockHash).toEqual('0')
    expect(block.hash).toEqual('0')

  })

  it('adds genesis block if present as argument in constructor', () => {
    const genesisBlock = Blockchain.createNewBlock(1, [], 100, '0', '0')
    const blockchain = new Blockchain(genesisBlock)

    expect(blockchain.chain.length).toEqual(1)
    const block1 = blockchain.chain[0]
    expect(block1.index).toEqual(1)
    expect(block1.nonce).toEqual(100)
    expect(block1.transactions).toEqual([])
    expect(block1.previousBlockHash).toEqual('0')
    expect(block1.hash).toEqual('0')
  })

  it('gets last block', () => {
    const genesisBlock = Blockchain.createNewBlock(1, [], 100, '0', '0')
    const blockchain = new Blockchain(genesisBlock)

    const block1 = blockchain.getLastBlock()

    expect(block1).toEqual(genesisBlock)

    blockchain.addTransaction(Blockchain.createNewTransaction(100, 'foo', 'bar'))
    blockchain.addTransaction(Blockchain.createNewTransaction(40, 'bar', 'baz'))
    blockchain.addNewBlock(1000, '0', '1234')

    const block2 = blockchain.getLastBlock()

    expect(block2.transactions.length).toEqual(2)
  })

  it('adds new blocks (without mining and hashes validation)', () => {
    const genesisBlockHash = '0'
    const genesisBlock = Blockchain.createNewBlock(1, [], 100, '0', genesisBlockHash)
    const blockchain = new Blockchain(genesisBlock)

    blockchain.addTransaction(Blockchain.createNewTransaction(100, 'foo', 'bar'))
    blockchain.addTransaction(Blockchain.createNewTransaction(40, 'bar', 'baz'))

    expect(blockchain.pendingTransactions.length).toEqual(2)
    expect(blockchain.chain.length).toEqual(1)

    blockchain.addNewBlock(1000, genesisBlockHash, '1234')

    expect(blockchain.pendingTransactions.length).toEqual(0)
    expect(blockchain.chain.length).toEqual(2)
    expect(blockchain.chain[1].transactions.length).toEqual(2)

    blockchain.addTransaction(Blockchain.createNewTransaction(99, 'baz', 'foo'))

    expect(blockchain.pendingTransactions.length).toEqual(1)
    expect(blockchain.chain[1].transactions.length).toEqual(2)

    blockchain.addNewBlock(1001, '1234', '5678')

    expect(blockchain.pendingTransactions.length).toEqual(0)
    expect(blockchain.chain.length).toEqual(3)
    expect(blockchain.chain[2].transactions.length).toEqual(1)
  })

})