const { uniqueId } = require('utils/uniqueId')

class Blockchain {
  constructor (genesisBlock = null) {
    this.chain = []
    this.pendingTransactions = []
    if (genesisBlock !== null) {
      this.chain.push(genesisBlock)
    }
  }

  static createNewTransaction (amount, sender, recipient) {
    const newTransaction = {
      amount: amount,
      sender: sender,
      recipient: recipient,
      transactionId: uniqueId()
    }

    return newTransaction
  }

  static createNewBlock (index, transactions, nonce, previousBlockHash, hash) {
    const newBlock = {
      index: index,
      timestamp: Date.now(),
      transactions: transactions,
      nonce: nonce,
      hash: hash,
      previousBlockHash: previousBlockHash
    }

    return newBlock
  }

  addTransaction (transaction) {
    this.pendingTransactions.push(transaction)
  }

  addNewBlock (nonce, previousBlockHash, hash) {
    const newBlock = Blockchain.createNewBlock(this.chain.length + 1, this.pendingTransactions, nonce, previousBlockHash, hash)
    this.pendingTransactions = []
    this.pushNewBlock(newBlock)
  }

  pushNewBlock (block) {
    this.chain.push(block)
  }

  getLastBlock () {
    return this.chain[this.chain.length - 1]
  }

}

module.exports = Blockchain