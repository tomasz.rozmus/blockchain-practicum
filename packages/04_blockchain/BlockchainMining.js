const { uniqueId } = require('utils/uniqueId')
const pow = require('utils/proofOfWork')
const fulfills = require('utils/fulfills')
const sha256 = require('utils/sha256')
const BlockchainSimple = require('./BlockchainSimple')

const DEFAULT_DIFFICULTY = '00ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff'

class Blockchain extends BlockchainSimple {
  constructor (genesisBlock = null) {
    super(genesisBlock)
    this.difficulty = DEFAULT_DIFFICULTY
  }

  static hashBlock (previousBlockHash, blockData, nonce) {
    return sha256(previousBlockHash + JSON.stringify(blockData) + nonce)
  }

  proofOfWork (previousBlockHash, currentBlockData) {
    const dataAsString = previousBlockHash + JSON.stringify(currentBlockData)
    return pow(dataAsString, this.difficulty)
  }

  mine () {
    const lastBlock = this.getLastBlock()
    const previousBlockHash = lastBlock.hash
    const currentBlockData = {
      transactions: this.pendingTransactions,
      index: lastBlock.index + 1
    }
    const nonce = this.proofOfWork(previousBlockHash, currentBlockData)
    const blockHash = Blockchain.hashBlock(previousBlockHash, currentBlockData, nonce)

    const newBlock = Blockchain.createNewBlock(currentBlockData.index, currentBlockData.transactions, nonce, previousBlockHash, blockHash)
    this.pushNewBlock(newBlock)
    this.pendingTransactions = []

    return newBlock
  }

  static isValidChain (chain) {
    let validChain = true

    for (let i = 1; i < chain.length; i++) {
      const currentBlock = chain[i]
      const prevBlock = chain[i - 1]
      const blockHash = Blockchain.hashBlock(prevBlock['hash'], {
        transactions: currentBlock['transactions'],
        index: currentBlock['index']
      }, currentBlock['nonce'])
      if (!fulfills(blockHash, DEFAULT_DIFFICULTY)) validChain = false
      if (currentBlock['previousBlockHash'] !== prevBlock['hash']) validChain = false
    }

    const genesisBlock = chain[0]
    const correctNonce = genesisBlock['nonce'] === 100
    const correctPreviousBlockHash = genesisBlock['previousBlockHash'] === '0'
    const correctHash = genesisBlock['hash'] === '0'
    const correctTransactions = genesisBlock['transactions'].length === 0

    if (!correctNonce || !correctPreviousBlockHash || !correctHash || !correctTransactions) validChain = false

    return validChain
  }

}

module.exports = Blockchain