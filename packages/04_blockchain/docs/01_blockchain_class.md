# The Blockchain
Let's start implementing our blockchain. We will start with a simple class with basic "offline" functions for:
* managing transactions queue
    * creating transaction object
    * adding transaction to pending transactions
* blocks and chain functions
    * creating a block
    * creating a genesis block
    * adding blocks to chain
    * performing PoW
    * validating chain

Exercise: Create `BlockchainSimple.js` class which satisfies `BlockchainSimple.test.js`

```
class Blockchain {
    constructor(genesisBlock = null) {
        // ...
    }
    // ... private and public methods
}

module.exports = Blockchain
```
