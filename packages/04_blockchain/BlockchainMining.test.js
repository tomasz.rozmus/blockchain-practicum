const Blockchain = require('./BlockchainMining')

describe('BlockchainMining', () => {

  it('performs proof of wWork', () => {
    const genesisBlock = Blockchain.createNewBlock(1, [], 100, '0', '0')
    const blockchain = new Blockchain(genesisBlock)

    expect(blockchain.difficulty).toEqual('00ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff')

    const t1 = Blockchain.createNewTransaction(100, 'foo', 'bar')
    const t2 = Blockchain.createNewTransaction(40, 'bar', 'baz')
    const blockData = {
      index: 1,
      transactions: [t1, t2]
    }
    const nonce = blockchain.proofOfWork('0', blockData)

    expect(typeof nonce).toEqual('number')
  })

  it('mines', () => {
    const genesisBlockHash = '0'
    const genesisNonce = 100
    const genesisBlock = Blockchain.createNewBlock(1, [], genesisNonce, '0', genesisBlockHash)
    const blockchain = new Blockchain(genesisBlock)

    expect(blockchain.chain.length).toEqual(1)

    blockchain.addTransaction(Blockchain.createNewTransaction(100, 'foo', 'bar'))
    blockchain.addTransaction(Blockchain.createNewTransaction(40, 'bar', 'baz'))

    expect(blockchain.pendingTransactions.length).toEqual(2)

    const minedBlock1 = blockchain.mine()

    expect(blockchain.pendingTransactions.length).toEqual(0)
    expect(blockchain.chain.length).toEqual(2)
    expect(blockchain.getLastBlock()).toEqual(minedBlock1)
    expect(blockchain.chain[0].hash).toEqual(blockchain.chain[1].previousBlockHash)

    blockchain.addTransaction(Blockchain.createNewTransaction(10, 'baz', 'bar'))

    expect(blockchain.pendingTransactions.length).toEqual(1)

    const minedBlock2 = blockchain.mine()

    expect(blockchain.pendingTransactions.length).toEqual(0)
    expect(blockchain.chain.length).toEqual(3)
    expect(blockchain.getLastBlock()).toEqual(minedBlock2)
    expect(blockchain.chain[1].hash).toEqual(blockchain.chain[2].previousBlockHash)


    const valid = Blockchain.isValidChain(blockchain.chain)

    expect(valid).toEqual(true)
  })

})