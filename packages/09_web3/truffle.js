const HDWalletProvider = require('truffle-hdwallet-provider')
const { MNEMONIC, INFURA_API_KEY } = require('./private.js')

console.log(MNEMONIC, INFURA_API_KEY)

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // for more about customizing your Truffle configuration!
  networks: {
    development: {
      host: '127.0.0.1',
      port: 7545,
      network_id: '*' // Match any network id
    },
    ropsten: {
      provider: function () {
        return new HDWalletProvider(MNEMONIC, `https://ropsten.infura.io/v3/${INFURA_API_KEY}`)
      },
      network_id: 3,
      gas: 4000000,      //make sure this gas allocation isn't over 4M, which is the max
      gasPrice: 10000000000,
      skipDryRun: true
    }
  }
}
