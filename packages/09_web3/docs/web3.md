# ERC20
The ERC20 token standard describes the functions and events that an Ethereum token contract has to implement (wikipedia).
https://theethereum.wiki/w/index.php/ERC20_Token_Standard

# Web3
Web3.js is a library allowing web applications to interact with the blockchain.

In ganache, web3 it is already present in global scope.

Type `truffle console`, then:
```
var accounts = await web3.eth.getAccounts()
var tx = await web3.eth.sendTransaction({from: accounts[0], to: accounts[1], value: web3.utils.toWei("0.3", 'ether')})
```
Then transfer some amount of ether yourself (you are on Ganache network, so your metamask main account has 0 ether).

Observe in ganache transactions and logs.

This is a small web application for transfering tokens.

Open Ganache and setup this project. Type: `truffle migrate` and then `yarn dev`. Play with it. Transfer some TT Token between your own accounts.

# Truffle deployment on ropsten network

In order to test contract behaviour more realistic way, it should be deployed on a test network. We need 2 things to achieve this
* wallet with signing capabilities (`truffle-hdwallet-provider`)
* test node api endpoint (Infura)

Truffle hdwallet needs your mnemonic in order to create and auto-sign transactions. Mnemonic should stay private so that no one steals your funds.


Deploy the contract on test network with   
`truffle migrate --network ropsten`

Excercise:
Add TT token to your MetaMask token list for main and second account. Transfer the token between accounts. Change rpc provider in `/src/js/app.js:17` to infura *with your api key*.
Start the app with `yarn start`, open the app in a browser and transfer funds.

Excercise:
Extend the app to display:
* your currently selected address. 
* your currently selected network. 

Excercise:
Create a 'Token Data' (visually like 'My Wallet') section with a result of `totalSupply` contract method call.
