const lowerCaseHexRegex = /[0-9a-f]/g
function regenerateHex (firstDigits) {
  const digits = firstDigits.toString().toLowerCase()
  if (!lowerCaseHexRegex.test(digits)) {
    throw Error('Not a hex digits')
  }
  return digits + 'f'.repeat(64 - digits.length)
}

module.exports.regenerateHex = regenerateHex

module.exports.getDifficultyHex = function (difficulty) {
  return regenerateHex(difficulty)
}