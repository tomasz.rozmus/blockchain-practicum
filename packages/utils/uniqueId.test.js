const { uniqueId } = require('./uniqueId')

describe('uniqueId', () => {
  it('works', () => {
    const id = uniqueId()
    expect(id).toEqual(expect.stringMatching(/^[0-9a-f]{32}$/i))
  })
})