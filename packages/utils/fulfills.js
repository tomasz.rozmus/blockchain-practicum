const { parseBigInt } = require('utils')

function fulfills (hash, difficulty) {
  const hashAsNumber = parseBigInt(hash, 16)
  const difficultyAsNumber = parseBigInt(difficulty, 16)
  return hashAsNumber - difficultyAsNumber < BigInt(0)
}

module.exports = fulfills
