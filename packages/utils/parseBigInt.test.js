const parseBigInt = require('./parseBigInt')

describe('parseBigInt', () => {
  it('works', () => {
    const bigHex = 'ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad'
    const result = parseBigInt(bigHex, 16).toString(10)
    const expected = '84342368487090800366523834928142263660104883695016514377462985829716817089965'
    expect(result).toEqual(expected)
  })

  it('handles basic math operations', () => {
    const bigHex = 'ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad'
    const bigInteger = parseBigInt(bigHex, 16)

    const tenTo74 = parseBigInt('100000000000000000000000000000000000000000000000000000000000000000000000000')
    const tenTo76 = parseBigInt('10000000000000000000000000000000000000000000000000000000000000000000000000000')
    const tenTo150 = parseBigInt('1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000')

    // addition
    const additionResult = bigInteger + tenTo76
    const expectedAdditionResult = '94342368487090800366523834928142263660104883695016514377462985829716817089965'
    expect(additionResult.toString()).toEqual(expectedAdditionResult)

    // subtraction
    const subtractionResult = bigInteger - BigInt('1')
    const expectedSubtractionResult = '84342368487090800366523834928142263660104883695016514377462985829716817089964'
    expect(subtractionResult.toString()).toEqual(expectedSubtractionResult)

    //multiplication
    const multiplicationResult = tenTo76 * tenTo74
    expect(multiplicationResult.toString()).toEqual(tenTo150.toString())
    expect(multiplicationResult === tenTo150).toEqual(true)
    expect(multiplicationResult).toEqual(tenTo150)

    // exponentiation
    const exponentiationResult = BigInt(10) ** BigInt(76)
    expect(exponentiationResult).toEqual(tenTo76)

    // division
    const divisionResult = tenTo76 / tenTo74
    expect(divisionResult).toEqual(BigInt(100))

    // division with rest
    const divisionResult2 = BigInt(5) / BigInt(2)
    expect(divisionResult2).toEqual(BigInt(2))
    expect(divisionResult2.toString(10)).toEqual('2')

  })

  it('handles comparisions', () => {
    const tenTo76 = parseBigInt('10000000000000000000000000000000000000000000000000000000000000000000000000000')
    const tenTo76plusOne = parseBigInt('10000000000000000000000000000000000000000000000000000000000000000000000000001')

    expect(tenTo76 < tenTo76plusOne).toEqual(true)
    expect(tenTo76 + BigInt(1) === tenTo76plusOne).toEqual(true)
    // expect(tenTo76 + 1 === tenTo76plusOne).toEqual(true) // this wont work since we use primitive number

    expect(tenTo76 > tenTo76plusOne).toEqual(false)
  })
})