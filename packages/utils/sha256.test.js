const sha256 = require('./sha256')
const parseBigInt = require('./parseBigInt')

describe('sha256', () => {
  it('works', () => {
    const data = 'abc'

    const result = sha256(data)

    // https://www.di-mgt.com.au/sha_testvectors.html https://www.movable-type.co.uk/scripts/sha256.html
    const expected = 'ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad'
    expect(result).toEqual(expected)
    // expect(result).toBeHash()
  })

  it('casts to BigInt', () => {
    const data = 'abc'

    const hashHex = sha256(data)
    const hashInt = parseBigInt(hashHex, 16)
    const casted = BigInt('0x' + hashHex)
    const decremented = casted - BigInt(1)

    expect(hashInt).not.toEqual(hashHex)
    expect(casted.toString(16)).toEqual(hashHex)
    const expectedDecrementedHex = 'ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ac'
    expect(decremented.toString(16)).toEqual(expectedDecrementedHex)
  })
})