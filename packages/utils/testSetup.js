const hexHashRegex = /[0-9a-fA-F]/g

const toBeHashMatcher = (received) => {
  const pass = hexHashRegex.test(received) && received.length === 64
  if (pass) {
    return {
      message: () =>
        `expected ${received} not to be hash`,
      pass: true,
    }
  } else {
    return {
      message: () =>
        `expected ${received} to be hash`,
      pass: false,
    }
  }
}

expect.extend({
    toBeHash: toBeHashMatcher
  }
)