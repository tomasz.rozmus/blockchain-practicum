const { regenerateHex } = require('./hex')

describe('regenerateHex', () => {
  it('works', () => {
    const starting = '0'
    const hex = regenerateHex(starting)
    expect(hex).toEqual('0fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff')
    expect(hex.length).toEqual(64)
  })

  it('works for mixed numbers', () => {
    const starting = '00000F1'
    const hex = regenerateHex(starting)
    expect(hex).toEqual('00000f1fffffffffffffffffffffffffffffffffffffffffffffffffffffffff')
    expect(hex.length).toEqual(64)
    expect(hex).toBeHash()
  })

  it('throws error for wrong digits', () => {
    const starting = '0xyz'
    let hex = ''
    const getHex = () => {
      hex = regenerateHex(starting)
    }
    expect(getHex).toThrow()
  })

})