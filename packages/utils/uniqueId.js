const uuid = require('uuid')

module.exports.uniqueId = () => uuid().split('-').join('')