const { sha256 } = require('utils')
const merkleTree = require('./merkleTree')

const getTx = (from, to, amount) => ({
  from,
  to,
  amount
})

describe('merkleTree', () => {
  const t1 = getTx('a', 'b', 100)
  const t2 = getTx('b', 'c', 99)
  const t3 = getTx('c', 'd', 9)
  const t4 = getTx('e', 'f', 10)
  const t5 = getTx('b', 'x', 12.2)

  it('works for single tx', () => {

    const data = [t1]
    const root = merkleTree(data)
    const hash = sha256(sha256(JSON.stringify(t1)) + sha256(JSON.stringify(t1)))
    expect(root).toEqual(hash)
    expect(root).toEqual('63b0c12640fedcea887291416f2e80134f1008e869a53aeb3c607ff039ca0624')
  })

  it('works for tx pair', () => {

    const data = [t1, t1]
    const root = merkleTree(data)
    const hash = sha256(sha256(JSON.stringify(t1)) + sha256(JSON.stringify(t1)))
    expect(root).toEqual(hash)
    expect(root).toEqual('63b0c12640fedcea887291416f2e80134f1008e869a53aeb3c607ff039ca0624')
  })

  it('works for tx pair 2', () => {
    const data = [t1, t2]
    const root = merkleTree(data)
    const hash = sha256(sha256(JSON.stringify(t1)) + sha256(JSON.stringify(t2)))
    expect(root).toEqual(hash)
    expect(root).toEqual('0b886d84c9a6b8926d20dfdb7d674af16e4c202385f8d36b04f65a2512f1c32e')
  })

  it('works for tx pair 3', () => {
    const data = [t3, t3]
    const root = merkleTree(data)
    const hash = sha256(sha256(JSON.stringify(t3)) + sha256(JSON.stringify(t3)))
    expect(root).toEqual(hash)
    expect(root).toEqual('bf0f6612630459fc3ce23baab42b1f4357e6ef9b25501365410de6d57c104853')
  })

  it('works for 3 tx', () => {
    const data = [t1, t2, t3]

    const root = merkleTree(data)

    const hashT1T2 = sha256(sha256(JSON.stringify(t1)) + sha256(JSON.stringify(t2)))
    const hashT3T3 = sha256(sha256(JSON.stringify(t3)) + sha256(JSON.stringify(t3)))
    const hash = sha256(hashT1T2 + hashT3T3)
    expect(root).toEqual(hash)
    expect(root).toEqual('a551c063706acdc81a16e2554c02b8edd6e997852162cf1df76ec3b0ae2e9d18')
  })

  it('works for 5tx', () => {
    const data = [t1, t2, t3, t4, t5]

    const root = merkleTree(data)

    const t1t2 = sha256(sha256(JSON.stringify(t1)) + sha256(JSON.stringify(t2)))
    const t3t4 = sha256(sha256(JSON.stringify(t3)) + sha256(JSON.stringify(t4)))
    const t5t5 = sha256(sha256(JSON.stringify(t5)) + sha256(JSON.stringify(t5)))
    const hash = sha256(sha256(t1t2 + t3t4) + sha256(t5t5 + t5t5))
    expect(root).toEqual(hash)
    expect(root).toEqual('825771dd3673a50183e545d04da3b1fd95f612c15a9db1de7f4fca3c6ee70350')
  })

})