const parseBigInt = (value, radix) => {
  switch (radix) {
    case 2:
      return BigInt('0b' + value)
    case 16:
      return BigInt('0x' + value)
    default:
      return BigInt(value)
  }
}

module.exports = parseBigInt