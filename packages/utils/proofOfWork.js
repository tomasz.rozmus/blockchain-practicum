const { sha256 } = require('utils')
const fulfills = require('utils/fulfills')

const MAX_ITERATIONS = Math.pow(10, 8)

function proofOfWork (data, difficulty) {
  for (let i = 0; i <= MAX_ITERATIONS; i++) {
    const nonce = Math.floor(Math.random() * MAX_ITERATIONS)
    const hash = sha256(data + nonce)
    if (fulfills(hash, difficulty)) { // hash is lower than difficulty
      return nonce
    }
  }
  throw Error('Maximum iterations reached')
}

module.exports.fullfils = fulfills
module.exports = proofOfWork